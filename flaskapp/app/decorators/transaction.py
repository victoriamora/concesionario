from functools import wraps

from app.database.commons_dao import commit, rollback, close, remove


def transactional(func):
	@wraps(func)
	def deco_transaction(*args, **kwargs):
		try:
			res = func(*args, **kwargs)
			commit()
			return res
		except Exception as e:
			rollback()
			raise e
		finally:
			close()
			remove()

	return deco_transaction