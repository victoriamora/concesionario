from functools import wraps

from flask_login import current_user
from werkzeug.exceptions import Unauthorized


def roles_required(*admited_roles):
	def decorator(f):
		@wraps(f)
		def wrap(*args, **kwargs):
			if current_user.role in admited_roles or current_user.role == 'admin':
				return f(*args, **kwargs)
			else:
				raise Unauthorized('Access Denied')
		return wrap
	return decorator