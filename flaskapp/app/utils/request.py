from flask import request


def get_param(key):
	return request.args.get(key)

def get_boolean_param(key):
	value = request.args.get(key, '')
	return value.lower() == 'true'