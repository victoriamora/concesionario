import locale
from datetime import datetime, timedelta

from config import BASE_DATE_FORMAT
import calendar

def date_to_string(date, format=BASE_DATE_FORMAT):
	return date.strftime(format)

def string_to_date(date, format=BASE_DATE_FORMAT, locale_lang=None):
	default_locale = locale.getlocale(locale.LC_TIME)

	if locale_lang:
		locale.setlocale(locale.LC_TIME, locale_lang)
		date_item = datetime.strptime(date, format)
		locale.setlocale(locale.LC_TIME, default_locale)
	else:
		date_item = datetime.strptime(date, format)
	return date_item

def get_current_date(as_string=False, format=BASE_DATE_FORMAT):
	date = datetime.utcnow()
	if as_string:
		return date_to_string(date, format=format)
	return date


def get_previous_period(start_date_str, end_date_str, format = BASE_DATE_FORMAT):
	start_date = string_to_date(start_date_str, format=format)
	end_date = string_to_date(end_date_str, format=format)
	days = abs(end_date - start_date).days

	prev_end_date = start_date - timedelta(days=1)
	prev_start_date = prev_end_date - timedelta(days=days)
	prev_end_date_str = date_to_string(prev_end_date, format=format)
	prev_start_date_str = date_to_string(prev_start_date, format=format)

	range_current = get_date_in_range(start_date_str, end_date_str)
	range_current.append(end_date_str)
	range_prev = get_date_in_range(prev_start_date_str, prev_end_date_str)
	range_prev.append(prev_end_date_str)

	index = 0
	relation = {}
	for d in range_current:
		relation[range_prev[index]] = d
		index += 1

	return prev_start_date_str, prev_end_date_str, relation

def get_date_in_range(startDate, endDate, format=BASE_DATE_FORMAT):
	'''Receives and returns Strings in d/m/y format'''
	result = []

	date1 = string_to_date(startDate, format)
	date2 = string_to_date(endDate, format)
	while date1 <= date2:
		currentDate = date_to_string(date1, format=format)
		result.append(currentDate)
		date1 = date1 + timedelta(days=1)

	return result

def get_week_to_date(date=get_current_date()):
	return date.isocalendar()[1]

def get_week_to_string_date(date=None, format=BASE_DATE_FORMAT):
	if date is None:
		return get_week_to_date()

	date_obj = string_to_date(date, format)
	return get_week_to_date(date_obj)

def get_month_name_to_date(date=get_current_date()):
	return date_to_string(date, format='%B')

def get_month_name_to_string_date(date=None, format=BASE_DATE_FORMAT):
	if date is None:
		return get_month_name_to_date()

	date_obj = string_to_date(date, format)
	return get_month_name_to_date(date_obj)

def get_month_abbr(month_number):
	return calendar.month_abbr[int(month_number)]

def get_quarter_to_date(date=get_current_date()):
	month = date.month
	return (month-1)//3 + 1

def get_quarter_to_string_date(date=None, format=BASE_DATE_FORMAT):
	if date is None:
		return get_quarter_to_date()

	date_obj = string_to_date(date, format)
	return get_quarter_to_date(date_obj)
