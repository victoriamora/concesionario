
class Object:

	def to_dict(self):
		res = []

		for name in dir(self.__class__):
			if name.startswith("_"):
				continue
			obj = getattr(self.__class__, name)
			if isinstance(obj, property):
				val = obj.__get__(self, self.__class__)
				res.append((name, val))

		# Its necesary load atrs in the end. For load instance
		attribs = [(k, v) for k, v in self.__dict__.items() if not k.startswith("_")]
		res.extend(attribs)

		return dict(res)

	def keys(self):
		return self.to_dict().keys()