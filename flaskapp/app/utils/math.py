
def calculate_avg(elems, prec=2):
	if not isinstance(elems, list):return None
	if len(elems) == 0: return 0
	if len(elems) == 1: return elems[0]

	return round(sum(elems)/len(elems), prec)