from uuid import uuid4

from app.utils.common_object import Object

PRIVATE_KEYS = ['pk', 'prefix', 'sufix', 'table']

class CommonModel(Object):
	__tablename__ = ''
	__mapper__ = None

	def __str__(self):
		return f'\n<Table: {self.table}>\n{self._print_values()}'

	def _print_values(self):
		res = ''
		for key, value in self.to_dict().items():
			res += f'{key}: {value} \n'
		return res

	@property
	def _private_keys(self):
		return PRIVATE_KEYS


	@property
	def prefix(self):
		return ''

	@property
	def sufix(self):
		return ''

	@property
	def table(self):
		return self.__tablename__

	@property
	def pk(self):
		try:
			return self.__mapper__.primary_key[0].name
		except:
			return ''

	def _generate_key(self):
		return self.prefix + str(uuid4().hex) + self.sufix

	def create_instance(self):
		if getattr(self, self.pk) is None:
			setattr(self, self.pk, self._generate_key())

		return self

	def to_dict(self):
		data = super(CommonModel, self).to_dict()
		return dict((k, v) for k, v in data.items() if k not in self._private_keys)

	def keys(self):
		data = super(CommonModel, self).keys()
		return [d for d in data if d not in self._private_keys]