from sqlalchemy import func

from app import db

def add_entity(entity):
	db.session.add(entity)

def commit():
	db.session.commit()

def rollback():
	db.session.rollback()

def delete(entity):
	db.session.delete(entity)

def close():
	db.session.close()

def remove():
	db.session.remove()

class BaseDAO:

	def __init__(self):
		self.entity = self.model()

	@property
	def session(self):
		return db.session

	@property
	def model(self):
		raise NotImplementedError

	@property
	def pk(self):
		return self.entity.pk

	def _write(self, entity, data=None):
		if data is None: data = {}

		change = False
		for key, value in data.items():
			if value is None:
				continue
			setattr(entity, key, value)
			change = True

		if change:
			add_entity(entity)
		return entity

	def insert(self, data):
		entity = self.model().create_instance()
		return self._write(entity, data)

	def update(self, entity, data):
		return self._write(entity, data)


	def get_all(self, as_dict=False):
		res = self.entity.query.all()
		if as_dict:
			return [x.to_dict() for x in res]
		return res

	def get_by_pk(self, candidate):
		filter_query = dict([(self.pk, candidate)])
		return self.entity.query.filter_by(**filter_query).first()

	def get(self, first=False, **kwargs):
		res = self.entity.query.filter_by(**kwargs)
		if first:
			return res.first()
		return res.all()

	def get_distinct_values_to_attrs(self, attr):
		res = self.session.query(getattr(self.model, attr).distinct()).all()
		return [r for r in res]

	def find(self, search, *fields, search_field=None):
		if search_field is None:
			search_field = fields[0]

		columns = []
		for field in fields:
			columns.append(
				getattr(self.model, field)
			)

		query = self.model.query.with_entities(*columns)
		for s in search.split(' '):
			qs = f"%{s}%"
			query = query.filter(getattr(self.model, search_field).like(qs))

		return query.all()

	def get_min_max(self, *fields, as_dict=False):
		select = []
		for column in fields:
			select.append(func.min(getattr(self.model, column)).label(f'{column}_min'))
			select.append(func.max(getattr(self.model, column)).label(f'{column}_max'))

		res = self.session.query(*select).first()
		if as_dict:
			return res._asdict()
		return res