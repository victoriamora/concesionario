from app import login
from app.database.commons_dao import BaseDAO
from .model import User

@login.user_loader
def load_user(id):
	return User.query.filter_by(id_user = id).first()

class UserDAO(BaseDAO):

	@property
	def model(self):
		return User

	def get_user_by_name(self, username):
		return self.get(username=username, first=True)