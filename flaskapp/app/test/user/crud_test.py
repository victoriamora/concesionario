import logging

from flask_login import logout_user

from app.blueprints.user.services.svc import UserServices
from app.database.user.dao import UserDAO
from app.database.user.model import User
from .mock import MOCK_USER_DATA
from ..common_test import TestCommon


class TestCrudUser(TestCommon):

	def _create_db(self):
		engine = self.get_db_engine()
		User.__table__.create(bind=engine, checkfirst=True)

	def _instance_dao(self):
		self.dao = UserDAO()

	def _instance_attrs(self):
		self.id_user = MOCK_USER_DATA.get('id_user')
		self.data = MOCK_USER_DATA

	def _create_data(self):
		UserServices(**self.data).register_user()
		logout_user()

	def test_update_user(self):
		self.title = 'Crud User - Update'
		self._init_test()

		user = self.dao.get_by_pk(self.id_user)
		logging.info(f'INIT DATA: {user}')

		user.username = 'anima'
		user.password = '123test'
		user.email = 'update@hotmail.com'

		UserServices(**self.data).update(user.to_dict())

		self.res = self.dao.get_by_pk(self.id_user)
		self._end_test()

	def _custom_teardown(self):
		engine = self.get_db_engine()
		User.__table__.drop(bind=engine, checkfirst=True)

