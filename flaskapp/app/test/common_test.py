import logging

from flask_testing import TestCase

from app import app, db

DB_CONNECTION = f'mysql+pymysql://root:testRules!24@localhost/tickets_seeker'

class TestCommon(TestCase):

	title = ''
	res = 'OK'

	@property
	def db_production(self):
		return False

	def create_app(self):
		app = self._set_config()
		return app

	def _create_db(self):
		pass

	def _instance_dao(self):
		pass

	def _instance_attrs(self):
		pass

	def _set_config(self):
		app.config['TESTING'] = True
		app.config['WTF_CSRF_ENABLED'] = False
		app.config['LOGIN_DISABLED'] = True

		if not self.db_production:
			app.config['SQLALCHEMY_DATABASE_URI'] = DB_CONNECTION

		return app

	def _print_init_test(self):
		logging.info(f'>>>>>>>>>>>>>>>>> INIT {self.title.upper()} TEST <<<<<<<<<<<<<<<<<')

	def _print_end_test(self):
		logging.info(f'>>>>>>>>>>>>>>>>> [FINISH] RES: {self.res} <<<<<<<<<<<<<<<<< \n\n\n')

	def _create_data(self):
		pass

	def _init_test(self):
		self._print_init_test()
		self._create_data()

	def _end_test(self):
		self._print_end_test()


	def get_db_engine(self):
		return db.engine

	def setUp(self):
		if not self.db_production:
			self._create_db()
		self._instance_dao()
		self._instance_attrs()

	def tearDown(self):
		db.session.remove()
		self._custom_teardown()

	def _custom_teardown(self):
		pass

	def print_log_test(self, text):
		logging.info(f'>>>>>>>>>>>>>>>>> CHECK: {text}')
