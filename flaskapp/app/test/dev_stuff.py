from app.decorators.transaction import transactional
from .common_test import TestCommon


class TestDevStuff(TestCommon):

	@property
	def db_production(self):
		return True

	def _instance_dao(self):
		pass

	@transactional
	def test_dev_stuff(self):
		self.title = 'Dev Stuff'
		self._init_test()

		# *. Your Code .* #

		self.res = 200
		self._end_test()
