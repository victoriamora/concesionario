from flask import Flask
from flask_cors import CORS
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from app import blueprints
from config import Config

db = SQLAlchemy()
login = LoginManager()

def create_app(config_class=Config):
	app = Flask(__name__, template_folder='../templates', static_folder='../static')
	app.config.from_object(config_class)

	db.init_app(app)
	login.init_app(app)
	login.login_view = 'login'

	# Register all Blueprints
	blueprints.register_blueprints(app)

	CORS(app)
	return app

app = create_app()
from . import common_routes

@app.shell_context_processor
def make_shell_context():
	return {'db': db}