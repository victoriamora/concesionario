from flask import jsonify, redirect, request
from flask_login import current_user, logout_user

from . import app
from .blueprints.user.services.svc import UserServices
from .decorators.templated import templated


@app.errorhandler(404)
def not_found_error(error):
	return jsonify({'status':'error', 'reason':'404', 'msg': error.description})

@app.errorhandler(500)
def internal_error(error):
	return jsonify({'status':'error', 'reason':'500', 'msg': error})

@app.route('/login', methods=['GET'])
@app.route('/login/', methods=['GET'])
@templated('login/login.html')
def login():
	if current_user.is_authenticated:
		return redirect('/index')

	return dict(error=False)

@app.route('/login', methods=['POST'])
@templated('login/login.html')
def sing_in():
	username = request.form.get('username')
	password = request.form.get('password')

	if not UserServices.sing_in(username, password):
		return dict(error=True)
	else:
		return redirect('/index')

@app.route('/register')
@templated('register/register.html')
def register():
	return dict()

@app.route('/logout')
def logout():
	logout_user()
	return redirect('/login')

@app.route('/', methods=['GET', 'POST'])
@app.route('/index')
@templated('index.html')
def index():
	return redirect('/search/manual')