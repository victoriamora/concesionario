
def register_blueprints(app):
	# Content Blueprint Register
	from .search import search
	app.register_blueprint(search)


	# User Blueprint Register
	from .user import user
	app.register_blueprint(user)