import json

from flask import request, redirect, jsonify
from flask_login import login_required
from werkzeug.exceptions import InternalServerError

from app.decorators.templated import templated
from app.utils.request import get_param
from . import user
from .services.svc import UserServices


@user.route('api/check-availability', methods=['GET'])
def check_availability():
	username = request.args.get('name')
	return {'available': UserServices.check_username_availability(username)}

@user.route('api/new', methods=['POST'])
@templated('crud/index.html')
def new():
	url_to_redirect = get_param('redirect')
	user_data = json.loads(json.dumps(request.form))
	res = UserServices(**user_data).register_user(login=False)
	if res is not None:
		return res
	return redirect(url_to_redirect)

@user.route('api/<user_id>')
def get_user_data(user_id):
	res = UserServices(id_user=user_id).get_data_user()
	return jsonify(res)

@user.route('api/update', methods=['POST'])
def update():
	user_data = request.get_json()
	res = UserServices().update(user_data)
	if res is None:
		raise InternalServerError('update user error')
	return res

@user.route('api/delete/<user_id>', methods=['DELETE'])
def user_delete(user_id):
	UserServices(id_user=user_id).delete()
	return jsonify(dict(ok=200))

