from app.blueprints.user.services.svc import UserServices
from app.decorators.templated import templated
from . import user


@user.route('/crud')
@templated('crud/index.html')
def user_crud_pages():
	users = UserServices().get_all_users()
	return dict(
		subtitle='Admin - User',
		users=users
	)