from flask import session
from flask_login import current_user, login_user
from werkzeug.exceptions import Forbidden, InternalServerError

from app.database.commons_dao import delete
from app.database.user.dao import UserDAO, load_user
from app.decorators.transaction import transactional
from app.session import session_manager
from app.utils.common_object import Object

class UserServices(Object):

	def __init__(self, **kwargs):

		self.id_user = kwargs.get('id_user')
		if self.id_user is not None and len(self.id_user) == 0:
			self.id_user = None

		self.username = kwargs.get('username')
		self.name = kwargs.get('name')
		self.lastname = kwargs.get('lastname')
		self.phone = kwargs.get('phone')
		self.password = kwargs.get('password')
		self.email = kwargs.get('email')
		self.salary = kwargs.get('salary')
		self.birth_date = kwargs.get('birth_date')
		self.timestamp_create = kwargs.get('timestamp_create')
		self.dao = UserDAO()

	def create_user(self):
		data = self.to_dict()
		return self.dao.insert(data)

	def get_all_users(self):
		return self.dao.get_all(as_dict=True)

	def get_data_user(self):
		entity = self.dao.get_by_pk(self.id_user)
		if not entity: return dict()
		return entity.to_dict()

	@transactional
	def register_user(self, login=True):
		if self.id_user:
			self.update(self.to_dict())
			return

		if not UserServices.check_username_availability(self.username):
			return dict(error=True, users=self.get_all_users())

		user = self.create_user()
		if user is None:
			raise InternalServerError('creating user error')

		if login:
			login_user(user)
			session_manager.set_session_parameters(session, user)

	@transactional
	def update(self, data):
		id_user = data.get('id_user')

		entity = self.dao.get_by_pk(id_user)

		if not data:
			return entity

		curret_password = data.get('curret_password')
		if curret_password:
			if not entity.check_password(curret_password):
				raise Forbidden('Incorrect Password')

		return self.dao.update(entity, data).to_dict()

	@transactional
	def delete(self):
		entity = self.dao.get_by_pk(self.id_user)
		delete(entity)

	@staticmethod
	def sing_in(username, password):
		user = UserServices.check_user_password(username, password)

		if not user:
			return False
		else:
			login_user(user)
			session_manager.set_session_parameters(session, user)
			return True

	@staticmethod
	def check_user_password(username, password):
		user = UserDAO().get_user_by_name(username)
		if user is None or not user.check_password(password):
			return
		else:
			return user

	@staticmethod
	def check_username_availability(username):
		return UserDAO().get_user_by_name(username) is None
