
# DECLARATE BLUEPRINT
from flask import Blueprint
search = Blueprint('search', __name__, template_folder='templates', url_prefix='/search')

from . import api
from . import routes