import json

from flask import request, jsonify

from app.blueprints.search import search
from app.blueprints.search.services.results import ResultsServices
from app.blueprints.search.services.search import SearchServices
from app.decorators.templated import templated


@search.route('/api/launch', methods=['POST'])
@templated('manual/index.html')
def launch_search():
	search_data = json.loads(json.dumps(request.form))
	search_data['offices'] = request.form.getlist('offices')
	search_data['sites'] = request.form.getlist('sites')

	data = SearchServices(**search_data).get_results()
	return ResultsServices(**search_data).manual_results(data)