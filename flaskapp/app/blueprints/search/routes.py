from app.decorators.templated import templated
from . import search


@search.route('/manual')
@templated('manual/index.html')
def search_manual():
	return dict(subtitle='Manual Search', filters={})