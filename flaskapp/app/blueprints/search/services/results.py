from app.utils.common_object import Object
from app.utils.math import calculate_avg


class ResultsServices(Object):

	def __init__(self, **kwargs):
		self.start_date = kwargs.get('start_date')
		self.start_time = kwargs.get('start_time')
		self.end_date = kwargs.get('end_date')
		self.end_time = kwargs.get('end_time')
		self.days = kwargs.get('days')
		self.offices = kwargs.get('offices')
		self.sites = kwargs.get('sites')

	def manual_results(self, data):

		grouped_sites = dict()
		grouped_date = dict()
		grouped_brand = dict()
		for d in data:
			site = d.get('site')
			date = d.get('date')
			brand = d.get('brand')
			price = d.get('price', 0)

			sites_data = grouped_sites.setdefault(site, [])
			sites_data.append(price)

			date_data = grouped_date.setdefault(date, {})
			date_site_data = date_data.setdefault(site, [])
			date_site_data.append(d.get('price', 0))

			grouped_brand.setdefault(brand, 0)
			grouped_brand[brand] += 1

		targets = []
		for site, prices in grouped_sites.items():
			targets.append(dict(
				site=site, price=calculate_avg(prices)
			))

		evolution = []
		for date, site_date in grouped_date.items():
			row = dict(date=date,)
			for site, prices in site_date.items():
				row[site] = calculate_avg(prices)

			evolution.append(row)

		brands = []
		for brand, offers in grouped_brand.items():
			brands.append(dict(
				brand=brand, offers=offers
			))

		return dict(
			subtitle='Manual Search',
			table=data,
			targets=targets,
			evolution=evolution,
			brand=brands,
			filters=self.to_dict()
		)