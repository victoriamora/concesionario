import random

from app.utils.common_object import Object
from app.utils.date_utils import get_date_in_range

CARS = dict(
	seat=['Ibiza', 'Ateca', 'Arona'],
	audi=['R8', 'A4', 'A3'],
	mercedes=['Clase A', 'Clase C', 'Clase E']
)

brands = list(CARS.keys())

class SearchServices(Object):

	def __init__(self, **kwargs):
		self.start_date = kwargs.get('start_date')
		self.start_time = kwargs.get('start_time')
		self.end_date = kwargs.get('end_date')
		self.end_time = kwargs.get('end_time')
		self.days = kwargs.get('days')
		self.offices = kwargs.get('offices')
		self.sites = kwargs.get('sites')

	def get_results(self):
		dates_range = get_date_in_range(self.start_date, self.end_date)

		res = []
		for date in dates_range:
			for office in self.offices:
				for site in self.sites:
					num_results = random.randint(1, 6)
					for r in range(num_results):
						brand = random.choice(brands)
						model = random.choice(CARS[brand])

						res.append(dict(
							date=date, model=model, brand=brand,
							office=office, site=site, price=round(random.uniform(55, 100) * float(self.days), 2)
						))

		return res