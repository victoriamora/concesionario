import os

from app import app

if __name__ == '__main__':
    # This is used when running locally only.
    app.run(host='127.0.0.1', port=8082, debug=False)