import {formatPrice} from "/static/js/utils/index.js";
import UserCrudForm from "./form.js";

const UserCrud = function () {
    return {
        init: function () {
            this.form = new UserCrudForm()

            this._init_table()
        },

        _init_table: function () {
            const $table = $('#user_list')

            $table.find('.salary').each((idx, elem) => {
                const value = elem.innerHTML
                elem.innerHTML = formatPrice(value)
            })

            //actions
            $table.on('click', '*[data-action="edit"]', e => {
                const data = $(e.currentTarget).data()
                fetch(`/user/api/${data.id}`)
                    .then(res => res.json())
                    .then(data => this.form.fill(data))
            })
            $table.on('click', '*[data-action="delete"]', e => {
                const data = $(e.currentTarget).data()
                fetch(`/user/api/delete/${data.id}`, {
                    method: 'DELETE'
                }).then(() => location.reload())
            })
        }
    }
}()

export default UserCrud
