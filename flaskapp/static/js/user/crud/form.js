import {getCurrentDate, subtractYear} from "/static/js/utils/index.js";

export default class UserCrudForm {
    constructor() {
        this.$form = $('#user_form')
        this.$userID = $('#id_user')
        this.$username = $('#username')
        this.$password = $('#password')
        this.$name = $('#name')
        this.$lastname = $('#lastname')
        this.$phone = $('#phone')
        this.$email = $('#email')
        this.$salary = $('#salary')
        this.$birth_date = $('#birth_date')

        //Max year for employees is 16
        const date = subtractYear(getCurrentDate(), 16)
        const minDate = date.toISOString().split('T')[0]
        this.$birth_date.attr('max', minDate);

        this.$form.on('click', '*[type="reset"]', () => this.clear())
    }

    clear() {
        this.$userID.val(null)
        this.$username.val(null)
        this.$password.val(null)
        this.$name.val(null)
        this.$lastname.val(null)
        this.$phone.val(null)
        this.$email.val(null)
        this.$salary.val(null)
        this.$birth_date.val(null)
    }

    fill(data) {
        this.$userID.val(data.id_user)
        this.$username.val(data.username)
        this.$name.val(data.name)
        this.$lastname.val(data.lastname)
        this.$phone.val(data.phone)
        this.$email.val(data.email)
        this.$salary.val(data.salary)
        const minDate = new Date(data.birth_date).toISOString().split('T')[0]
        this.$birth_date.val(minDate)
    }
}