import SearchManualForm from "./form.js";
import EvolutionChart from "./evolution.js";
import BrandChart from "./brand.js";

const SearchManual = function () {
    return {
        init: function() {
            this.form = new SearchManualForm()
            this.form.fill(filters)

            this.evolution = new EvolutionChart()
            this.evolution.show(evolution)

            this.brand = new BrandChart()
            this.brand.show(brand)
        }
    }
}()

export default SearchManual