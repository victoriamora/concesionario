export default class BrandChart {
    constructor() {
        this.selector = '#brand_chart'

        this.options = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: {
                top: 0, right: 5, bottom: 0, left: 0
            }
        }
    }

    show(data) {
        const values = data.map(d => d.offers)

        const chartData = {
            labels: data.map(d => d.brand),
            series: [values]
        }

        this.options.high = Math.ceil(Math.max(...values)/100) * 100

        Chartist.Bar(this.selector, chartData, this.options, [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ]);
    }
}