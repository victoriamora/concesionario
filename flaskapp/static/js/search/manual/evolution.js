export default class EvolutionChart {
    constructor() {
        this.selector = '#evolution_chart'

        this.options = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 200,
            chartPadding: {
                top: 0, right: 0, bottom: 0, left: 0
            }
        }
    }

    show(data) {
        const site1 = data.map(d => d['Site 1']),
            site2 = data.map(d => d['Site 2']),
            site3 = data.map(d => d['Site 3'])


        const chartData = {
            labels: data.map(d => d.date),
            series: [site1, site2, site3]
        }

        this.options.high = Math.ceil(Math.max(...site1, ...site2, ...site3)/100) * 100 + 20

        new Chartist.Line(this.selector, chartData, this.options);
    }
}