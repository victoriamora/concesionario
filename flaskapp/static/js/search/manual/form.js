import {getCurrentDate, addDays, isEmpty} from "/static/js/utils/index.js";

export default class SearchManualForm {
    constructor() {
        this.$form = $('#search_form')
        this.$startDate = $('#start_date')
        this.$startTime = $('#start_time')
        this.$endDate = $('#end_date')
        this.$endTime = $('#end_time')
        this.$days = $('#days')
        this.$offices = $('#offices')
        this.$sites = $('#sites')

        const minDate = addDays(getCurrentDate(), 1).toISOString().split('T')[0]
        this.$startDate.attr('min', minDate)
        this.$startDate.val(minDate)
        this.$endDate.attr('min', minDate)
        this.$endDate.val(minDate)

        this.$startTime.val('10:00')
        this.$endTime.val('10:00')
    }

    fill(data) {
        if (!isEmpty(data)) {
            this.$startDate.val(data.start_date)
            this.$startTime.val(data.start_time)
            this.$endDate.val(data.end_date)
            this.$endTime.val(data.end_time)
            this.$days.val(data.days)
            this.$offices.val(data.offices)
            this.$sites.val(data.sites)

        }
    }
}