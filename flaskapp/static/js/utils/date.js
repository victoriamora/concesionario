//Generate date
export const getCurrentDate = () => new Date()

//Subtractors
export const subtractYear = (date, years) => new Date(date.setYear(date.getFullYear() - years))

//Addons
export const addDays = (date, days) => new Date(date.setDate(date.getDate() + days))


