
export const formatPrice = (value, currency='EUR') =>
    new Intl.NumberFormat("es-ES", {
        style: "currency", currency: currency
    }).format(value)