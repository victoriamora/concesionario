import logging
import os

logging.basicConfig(level=logging.DEBUG)
basedir = os.path.abspath(os.path.dirname(__file__))

VERSION = 1
BASE_DATE_FORMAT = '%Y-%m-%d'

DB_USER = 'root'
DB_PSWD = ''
DB_HOST = 'localhost'
DB_SCHEMA = 'taller'

class Config:
	SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{DB_USER}:{DB_PSWD}@{DB_HOST}/{DB_SCHEMA}'
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SQLALCHEMY_COMMIT_ON_TEARDOWN = False

	SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess-1111223456781'